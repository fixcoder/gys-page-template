// const materialLastAndThis = require('./src/mock/menus.json')
const publicCdnConfig = require('./cdn.config')
const targetUrl = 'http://192.168.153.44' // 接口代理的ip地址
// 接口代理的cookie，有时效性，需要先登录代理地址，将cookie复制过来
const cookie =
  '__gm_ici=759f6ce5501544f989454b01f8e8d332; __gm.t=eyJhbGciOiJIUzUxMiJ9.WlhsS2FHSkhZMmxQYVVwcllWaEphVXhEU214aWJVMXBUMmxLUWsxVVNUUlJNRXBFVEZWb1ZFMXFWVEpKYmpBdUxuVkhkVVJVYlVVd2JHUjNabk0xWkZNME0wcHFkRkV1V2xwSVIwODViMnhDUTJweFRWTldSRVZoTW5sSk5HbEZkMEprTFZKM1NrVnhaRWx3TFdZMGJXMTBObDk0UlRSQ00zQk9lRk5vV1VsVFVqRk1hamRvUTAxS1ZHVmtTM0l0Y25KVE5uQldZbTVyUkhnNFFVNHlObkZxU25wVVVsQnpXRzVIVG0xM2VscENRVlIwVDNSdmJIWmxZelZGTVRaUGFFUnZRbEpQZFhwV2QyRmtaMDFTUzBsa1JuTm5aVVphTVUxYU4wUlFUR2h1YVhwZk9FeGlhMmxWVTBobFMybEdXbUZFUjFFelZtRndVVzFPUm5sWmIxUnNMWHB4YlZoSFoydEtOM0J5WkhOVmVrUk1SVXB4VTJ0NVozRjVSakl5YUV0blIwcHBjemd3U2xodk5tMXNPVkZ5Um1OT1JFTnRXV2N6WlU1RE4zZDRSM3AwUzAxS1dHNWZVbVZPWjBSd2RpMDVVVVIwY1ZZeGFWbFRWMUV1TUhaMlZIUnZZWFl0UVVGeFduaEdORVJGTVY4elFRPT0.ZriAvGPjvZL6Dyp3ic21q6DIvkCkbC1y8ZC9pZ-eH0rrdNeoDUx9lBA9EGJUNRggZjgxL8JqDsefIEF2zFp5_w; .JAVA.CLOUD.AUTH=54accdbb-cb99-43d9-a4b0-91de635d705c; .CLOUD_ACCESS_TOKEN=cn-a662d903-fb2d-4d0e-a5cd-3a2085ef8d00; .ENDPOINT=WEB; .CLOUDT_TENANT_ID=1454006; rootURL=/inspection/; org=1458545; project=; userOrg="[{"orgId":1458545,"userId":294243986764288}]"; cloudt_product_app_id=; .GYS.CLOUD.TOKEN='
module.exports = {
  publicPath: './',
  // transpileDependencies: ['gys-page-frame'],  //依赖包里面需要做语法转换的模块
  // 下面是多页面打包的相关配置
  pages: {
    home: {
      entry: 'src/views/home/main.js', // page 的入口
      template: 'public/index.html', // 模板来源
      filename: 'index.html', // 在 dist/index.html 的输出
      // 当使用 title 选项时，
      // template 中的 title 标签需要是 <title><%= htmlWebpackPlugin.options.title %></title>
      title: '物料验收首页',
      cdnConfig: publicCdnConfig
    },
    domo: {
      entry: 'src/views/demo/main.js', // page 的入口
      template: 'public/index.html', // 模板来源
      filename: 'demo.html', // 在 dist/demo.html 的输出
      // 当使用 title 选项时，
      // template 中的 title 标签需要是 <title><%= htmlWebpackPlugin.options.title %></title>
      title: '物料验收首页',
      cdnConfig: publicCdnConfig
    }
  },
  configureWebpack: {
    externals: {
      vue: 'Vue',
      'vue-router': 'VueRouter',
      vuex: 'Vuex'
    },
    performance: {
      hints: false
    }
  },
  devServer: {
    before: function (app, server) {
      // 获取菜单信息
      // app.get('/gys/system/permission/info', function (req, res) {
      //   res.json(materialLastAndThis)
      // })
    },
    proxy: {
      '/wl/': {
        target: targetUrl,
        changeOrigin: true,
        headers: {
          cookie
        }
      },
      '/gys/': {
        target: targetUrl,
        changeOrigin: true,
        headers: {
          cookie
        }
      },
      '/pmpp/': {
        target: targetUrl,
        changeOrigin: true,
        headers: {
          cookie
        }
      },
      '/cas/': {
        target: targetUrl,
        changeOrigin: true,
        headers: {
          cookie
        }
      }
    }
  }
}
